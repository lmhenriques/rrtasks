class TagsController < ApplicationController
  before_action :set_tag, only: %i[ show edit update destroy ]

  # GET /tags or /tags.json
  def index
    @tags = Tag.all
  end

  # GET /tags/1 or /tags/1.json
  def show
  end

  # GET /tags/new
  def new
    @tag = Tag.new
  end

  # GET /tags/1/edit
  def edit
  end

  # POST /tags or /tags.json
  def create
    @tag = Tag.new(tag_params)

    begin
      @tag.save!
      redirect_to tags_path, notice: "Tag was successfully created." and return

    rescue
      render :new, status: :unprocessable_entity and return
    end

  end

  # PATCH/PUT /tags/1 or /tags/1.json
  def update
    begin
      @tag.update!(tag_params)
      redirect_to tags_path, notice: "Tag was successfully updated." and return

    rescue
      render :edit, status: :unprocessable_entity and return
    end
  end

  # DELETE /tags/1 or /tags/1.json
  def destroy
    begin
      @tag.destroy!
      redirect_to tags_url, notice: "Tag was successfully destroyed." and return
    rescue
      redirect_to tags_url, notice: "Error: Unable to delete tag" and return
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_tag
      @tag = Tag.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def tag_params
      params.require(:tag).permit(:name)
    end
end
