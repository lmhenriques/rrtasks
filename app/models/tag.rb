# == Schema Information
#
# Table name: tags
#
#  id         :integer          not null, primary key
#  name       :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Tag < ApplicationRecord
  has_many :tags_task
  has_many :tasks, through: :tags_task # Using join model TagsTask

  validates :name, presence: true
end