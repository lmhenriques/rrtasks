require "dry/transaction"
require "validations/task_validation"

# Each transaction returns a result value wrapped in a Success or Failure object,
# based on the output of its final step. You can handle these results (including
# errors arising from particular steps) with a match block.
class TaskTransaction
  include Dry::Transaction # Allows this class to perform a series of steps

  # Steps to be performed (in order)
  step :validate
  step :clean
  step :update_current_date_new_cycle
  step :persist

  private
    # validates the incoming input
    # returns Success(valid_data) or Failure(validation)
    def validate(input)
      
      # NOTE: Cleaning incoming array which comes with a "" value on the first index
      input[:task][:tag_ids] = input[:task][:tag_ids]&.reject(&:empty?) || []

      # Date also comes as ""
      if input[:task][:date] == ""
        input[:task].delete :date
      end

      # TODO: Is this good practice?
      # NOTE: parameters MUST be permitted and hashed
      params = input.require(:task).permit(:title, :description, :date, :is_done, :set_repeat, :repeat, :tag_ids=>[])
      result = TaskValidation.new.call(params.to_h)

      # TODO: Send array of messages to failure! Not the object. Else it can overflow!
      # NOTE: result must be hashed in order to change its values in the next steps
      result.success? ? Success(result.to_h) : Failure(result.errors.to_h)
    end

    # changes the input to fit the task's model
    def clean(input)
      # We do not keep the "set_repeat" flag in the database
      # the idea is that models with "0" as repeat value will not repeat
      if (!input[:set_repeat])
        input[:repeat] = 0
      end
      Success(input)
    end

    # updating task's repetition cycle, if appropriate
    def update_current_date_new_cycle(input)
      # if a task is to be repated and was flagged as complete
      if input[:set_repeat] == true && input[:is_done].present? && input[:is_done]
        input[:date] = Date.parse( input[:date].to_s ).advance(days: input[:repeat]) # NOTE: We are guaranteed to have a date because "set_repeat" is true
        input[:is_done] = false # reset "complete" state
      end
      input.delete :set_repeat # removing param after using it to update cycle
      Success(input)
    end

    def persist(input, task_list, task)

      if task.id.present? # if we have a set id, we update the model
        begin
          task.update!(input.to_h)
          Success(input)
        rescue StandardError => e
          Failure(e.message)
        end

      else # if not, we create a new model
        begin
          task_list.tasks.create!(input.to_h)
          Success(input)
        rescue StandardError => e
          Failure(e.message)
        end
      end
    end
end