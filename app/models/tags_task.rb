# == Schema Information
#
# Table name: tags_tasks
#
#  tag_id  :integer          not null
#  task_id :integer          not null
#
class TagsTask < ApplicationRecord

  # NOTE: With join table models, naming convention is REALLy important
  # Use Alphabetical order and first model PLURAL (tags) and second model SINGULAR (task),
  # hence TagsTask
  # The same principle is applied to the file name, but using snake case: tags_task.rb

  belongs_to :tag
  belongs_to :task
end
