class TaskListsController < ApplicationController
  include TaskListsHelper

  before_action :set_task_list, only: %i[ show edit update destroy ]
  before_action :get_tag_list, only: %i[ show ]

  # GET /task_lists or /task_lists.json
  def index
    @task_lists = TaskList.all
  end

  # GET /task_lists/1 or /task_lists/1.json
  def show
    # checks if params[:task_list] exists. Then checks if params[:task_list][:tag_ids]
    # and rejects empty values. If these conditions are not present, sets "tags" to
    # an empty array
    tags = (params[:task_list] && params[:task_list][:tag_ids]&.reject(&:empty?)) || []
    chosen_tags = Tag.where(id: tags)

    @tasks = get_filtered_tasks(chosen_tags, @task_list).paginate(page: params[:page], per_page: 6)

    @task = Task.new
  end

  # GET /task_lists/new
  def new
    @task_list = TaskList.new
  end

  # GET /task_lists/1/edit
  def edit
  end

  # POST /task_lists or /task_lists.json
  def create
    begin
      @task_list = TaskList.create!(task_list_params)
      redirect_to task_list_url(@task_list), notice: "Task list was successfully created." and return
    rescue
      render :new, status: :unprocessable_entity and return
    end
  end

  # PATCH/PUT /task_lists/1 or /task_lists/1.json
  def update
    begin
      @task_list.update!(task_list_params)
      redirect_to task_list_url(@task_list), notice: "Task list was successfully updated." and return
    rescue
      render :edit, status: :unprocessable_entity and return
    end
  end

  # DELETE /task_lists/1 or /task_lists/1.json
  def destroy
    begin
      @task_list.destroy!
      redirect_to task_lists_url, notice: "Task list was successfully destroyed." and return
    rescue
      redirect_to task_lists_url, notice: "Unable to destroy task list" and return
    end
  end

  private
    def get_tag_list
      @tags = Tag.all
    end

    def set_task_list
      @task_list = TaskList.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def task_list_params
      params.require(:task_list).permit(:title)
    end
end
