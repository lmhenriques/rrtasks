# == Schema Information
#
# Table name: tasks
#
#  id           :integer          not null, primary key
#  title        :string           not null
#  description  :string           not null
#  date         :datetime
#  created_at   :datetime         not null
#  updated_at   :datetime         not null
#  task_list_id :integer          not null
#  is_done      :boolean          default(FALSE)
#  repeat       :integer          default(0)
#
class Task < ApplicationRecord
  belongs_to :task_list
  has_many :tags_task
  has_many :tags, through: :tags_task # Using join model TagsTask

  validates :title, presence: true
  validates :description, presence: true
  validates :task_list, presence: true

  # Checks if the current task is overdue
  def is_overdue
    (date && Time.zone.at(date).past?) || false
  end
end
