require "dry/validation"

class TaskValidation < Dry::Validation::Contract

  # NOTE: Using "params" instead of "schema" so it converts params to the
  # expected data types
  params do
    required(:title).filled(:string)
    required(:description).filled(:string)
    optional(:date).value(:date)
    optional(:is_done).value(:bool)
    optional(:set_repeat).value(:bool) # Task repetition ON / OFF
    optional(:repeat).value(:integer) # The repeat cycle in days
    optional(:tag_ids).value(:array).each(:integer) # Ids of the tags associated with this Task
  end

  # To repeat a task, user must select a date and a valid number of repetition days
  # NOTE: set_repeat can be present and be False, so we must check for Trueness
  rule(:set_repeat) do
    key.failure('must have a date') if values[:set_repeat] && !values[:date].present?
    key.failure('must have a valid number of days') if values[:set_repeat] && (!values[:repeat].present? || !values[:repeat].positive?)
  end

end