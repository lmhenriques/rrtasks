module TaskListsHelper

  # Returns a list of tasks filtered by tags and task list
  def get_filtered_tasks(tags, task_list)

    if tags.length.positive? # Checking for positive values
      tasks = []

      for tg in tags do
        # |= adds an element to the array unless it is akready present
        # The element must be wrapped in an array, that's why it works as an "add all"
        # by using a collection (tg.tasks)
        tasks |= tg.tasks.where(task_list: task_list) # relative to task list
      end

    else
      tasks = task_list.tasks
    end

    tasks
  end

end