class AddRepeatingToTasks < ActiveRecord::Migration[7.0]
  def change
    add_column :tasks, :repeat, :integer, default: 0
  end
end
