class TasksController < ApplicationController

  require "validations/task_transaction"

  before_action :set_task, only: %i[ edit update destroy ]
  before_action :get_tag_list, only: %i[ edit ]

  def edit
    @task_list = @task.task_list
  end

  # POST /tasks or /tasks.json
  def create
    task_list = TaskList.find(params[:task_list_id])

    TaskTransaction.new.with_step_args(persist: [task_list, Task.new]).call(params) do |on|
      on.success { |t| redirect_to task_list_path(task_list), notice: "Task was created successfully." and return }
      on.failure(:validate) { |v| redirect_to task_list_path(task_list), notice: v and return }
      on.failure { |e| redirect_to task_list_path(task_list), notice: e and return }
    end
  end

  # PATCH/PUT /tasks/1 or /tasks/1.json
  def update
    task_list = @task.task_list

    TaskTransaction.new.with_step_args(persist: [task_list, @task]).call(params) do |on|
      on.success { |t| redirect_to task_list_path(task_list), notice: "Task was successfully updated." and return }
      on.failure(:validate) { |v| redirect_to edit_task_list_task_path(task_list_id: task_list.id, id: @task.id), notice: v and return }
      on.failure { |e| redirect_to edit_task_list_task_path(task_list_id: task_list.id, id: @task.id), notice: e and return }
    end

  end

  # DELETE /tasks/1 or /tasks/1.json
  def destroy
    @task_list = @task.task_list
    begin
      @task.destroy!
      redirect_to task_list_path(@task_list), notice: "Task deleted" and return
    rescue
      redirect_to task_list_path(@task_list), notice: "Error: Unable to delete task" and return
    end
  end

  private
    def get_tag_list
      @tags = Tag.all
    end

    def set_task
      @task = Task.find(params[:id])
    end


    # Only allow a list of trusted parameters through.\
    # These params match the ones passed from the view
    # NOTE: tag_ids as array
    # NOTE: Responsibility passed to TaskTransaction
    # def task_params
    #   params.require(:task).permit(:title, :description, :date, :is_done, :repeat, :tag_ids=>[])
    # end

end