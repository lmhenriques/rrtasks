# == Schema Information
#
# Table name: task_lists
#
#  id         :integer          not null, primary key
#  title      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class TaskList < ApplicationRecord
  has_many :tasks, dependent: :destroy

  validates :title, presence: true, length: { minimum: 1 }
end
